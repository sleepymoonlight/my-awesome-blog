import React, {Component} from "react";
import {Route, Switch} from 'react-router'
import PostsPage from "./scenes/PostsPage/PostsPage";
import {EditPage} from "./scenes/EditPage/EditPage";
import connect from "react-redux/es/connect/connect";
import {getPostsAction} from "./actions/posts/getPostsAction";
import { withRouter } from 'react-router-dom';


class App extends Component {
    componentDidMount() {
        this.props.getPostsAction();
    }

    render() {
        return (
            <div className="App">
                <Switch>
                    <Route exact path="/" component={PostsPage}/>
                    <Route path="/posts-page" component={PostsPage}/>
                    <Route path="/editPost/:id" component={EditPage}/>
                </Switch>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        posts: state.posts
    };
}

export default withRouter(connect(
    mapStateToProps,
    {getPostsAction},
)(App))

