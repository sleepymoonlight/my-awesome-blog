import React from 'react';
import PropTypes from 'prop-types';
import Link from "react-router-dom/es/Link";

import styles from './Button.module.css';

class Button extends React.Component {
    render() {
        const buttonClass = `${styles.button} ${(this.props.buttonAction === 'edit')
        || (this.props.buttonAction === 'save')
            ? styles.buttonEdit
            : ((this.props.buttonAction === ('delete')
                    || (this.props.buttonAction === 'cancel'))
                    ? styles.buttonDelete
                    : ((this.props.buttonAction === 'showComments')
                            ? styles.buttonShowComments
                            : ''
                    )
            )
            }`;
        return (
            this.props.href
                ? <Link
                    className={buttonClass}
                    onClick={this.props.onClick}
                    to={this.props.href}
                >
                    {this.props.children}
                </Link>
                : <button
                    className={buttonClass}
                    onClick={this.props.onClick}
                    type={this.props.type}
                >
                    {this.props.children}
                </button>
        );
    }
}

Button.propTypes = {
    buttonAction: PropTypes.string.isRequired,
    href: PropTypes.string,
    onClick: PropTypes.func,
    type: PropTypes.string
};

export default Button;
