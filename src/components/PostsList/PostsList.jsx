import React, {Component} from 'react';
import {connect} from 'react-redux';

import {getPostsAction} from "../../actions/posts/getPostsAction";
import {deletePostAction} from "../../actions/posts/deletePostAction";
import {showActivePosts} from "../../actions/posts/postPaginationAction";
import {editPost} from "../../actions/posts/editPostAction";


import styles from './PostsList.module.css';
import Post from '../Post/Post';
import Navigation from "../Navigation/Navigation";

export class PostsList extends Component {
    constructor(props) {
        super(props);
        this.state = {activePage: 0, postsOnPage: 20};

        this.deletePost = this.deletePost.bind(this);
        this.goOnPage = this.goOnPage.bind(this);
    }

    componentDidMount() {
        if (this.props.articles.length === 0 && this.props.posts.length !== 0) {
            this.props.showActivePosts(this.state.activePage, this.state.postsOnPage);
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.articles.length === 0 && this.props.posts.length !== 0) {
            this.props.showActivePosts(this.state.activePage, this.state.postsOnPage);
        }

    }

    deletePost(post) {
        this.props.deletePostAction(post);
        this.props.showActivePosts(this.state.activePage, this.state.postsOnPage);
    }

    goOnPage(page) {
        if(page >= 0 && page <= this.props.maxPages - 1){
            const postsOnPage = this.state.postsOnPage;
            this.setState({activePage: page});
            this.props.showActivePosts(page, postsOnPage);
        }
    }

    render() {
        return (
            <React.Fragment>
                <Navigation goOnPage={this.goOnPage}
                            currentPage={this.state.activePage}
                            maxPages={this.props.maxPages}/>
                {this.props.articles.map(el => (
                    <div key={el.id} className={styles.post}>
                        <Post post={el} deletePostAction={this.deletePost}/>
                    </div>
                ))}
                <Navigation goOnPage={this.goOnPage}
                            currentPage={this.state.activePage}
                            maxPages={this.props.maxPages}/>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        articles: state.activePosts,
        maxPages: state.maxPages,
        posts: state.posts
    };
}

export default connect(
    mapStateToProps,
    {getPostsAction, showActivePosts, deletePostAction, editPost},
)(PostsList);
