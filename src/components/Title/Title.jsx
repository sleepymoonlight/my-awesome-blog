import React from 'react';
import PropTypes from 'prop-types';
import styles from './Title.module.css'

class Title extends React.Component {
    render() {
        const titleClass = `${styles.title} ${(this.props.isItMainTitle) ? styles.largeTitle : ''}`;
        return (
            <div className={titleClass}>
                {this.props.children}
            </div>
        )
    }
}

Title.propTypes = {
    isItMainTitle: PropTypes.bool.isRequired,
};


export default Title;
