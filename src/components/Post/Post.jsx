import React, {Component} from 'react';

import {history} from '../../store';

import Button from '../Button/Button';
import Title from '../Title/Title';
import Comment from '../Comment/Comment';

import styles from './Post.module.css';
import PropTypes from 'prop-types';
import {showComments} from "../../actions/comments/getCommentsAction";

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: {},
      isCommentsOpened: false,
    };
  }

  deletePost() {
    this.props.deletePostAction(this.props.post);
  }

  static editCurrentPost(id) {
    history.push('/editPost/' + id);
  }

  showComments(id) {
    if (this.state.isCommentsOpened) {
      this.setState({isCommentsOpened: false});
      return;
    }

    showComments(id).then(comments => {
      this.setState({comments: comments, isCommentsOpened: true});
    }).catch(error => {
      this.setState({comments: {}, isCommentsOpened: false});
    });
  }

  render() {
    const post = this.props.post;
    return (
        <div>
          <Title isItMainTitle={false}>* {post.title} *</Title>
          <p className={styles.postBody}>{post.body}</p>
          <div className={styles.postManage}>
            <div className={styles.leftAlight}>
              <Button buttonAction="edit"
                      onClick={Post.editCurrentPost.bind(this, post.id)}
              >EDIT</Button>
              <Button buttonAction="delete"
                      onClick={this.deletePost.bind(this)}
              >DELETE</Button>
            </div>
              {this.state.isCommentsOpened
                  ? <Button buttonAction="showComments"
                            onClick={this.showComments.bind(this, post.id)}
                  >HIDE COMMENTS</Button>
                  : <Button buttonAction="showComments"
                            onClick={this.showComments.bind(this, post.id)}
                  >SHOW COMMENTS</Button>}
          </div>
          {this.state.isCommentsOpened
              ? <Comment comments={this.state.comments}/>
              : <div/>}
        </div>
    );
  }
}

Post.propTypes = {
  post: PropTypes.object.isRequired,
  deletePostAction: PropTypes.func.isRequired,
};

export default Post;
