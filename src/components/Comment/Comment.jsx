import React, {Component} from 'react';

import styles from "./Comment.module.css"
import PropTypes from 'prop-types';

class Comment extends Component {
    render() {
        return (
            <div>
                {this.props.comments.map(el => (
                    <div key={el.id} className={styles.container}>
                        <p className={styles.title}>{el.name}</p>
                        <p className={styles.text}>{el.body}</p>
                        <a className={styles.email} href={el.email}>{el.email}</a>
                    </div>
                ))}
            </div>
        );
    }
}

Comment.propTypes = {
    comments: PropTypes.array.isRequired
};

export default Comment;
