import React from 'react';
import {connect} from 'react-redux';

import {getCurrentPost} from '../../actions/posts/getCurrentPostAction';
import {editPost} from "../../actions/posts/editPostAction";
import {history} from '../../store';
import Button from "../../components/Button/Button";

import styles from "./Form.module.css";

export class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = { title: "", body: ""};

      this.titleIsChanged = this.titleIsChanged.bind(this);
      this.postIsChanged = this.postIsChanged.bind(this);
    }

    componentDidMount() {
        this.props.getCurrentPost(this.props.id)
            .then(post => {this.setState({title: this.props.post.title, body: this.props.post.body})});
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.editPost(this.props.post.id, this.state.title, this.state.body);
        history.push('/');
    };

    titleIsChanged = (event) => {
        this.setState({title: event.target.value});
    };

    postIsChanged = (event) => {
      this.setState({body: event.target.value})
    };

    render() {

        return (
            <form onSubmit={this.handleSubmit}>
                <div className={styles.formContent}>
                    <input
                        type="text"
                        className={styles.titleInput}
                        onChange={this.titleIsChanged}
                        value={this.state.title}
                    />
                    <textarea
                        cols="30"
                        rows="10"
                        className={styles.bodyInput}
                        onChange={this.postIsChanged}
                        value={this.state.body}
                    />
                    <div className={styles.formManage}>
                        <Button buttonAction="save">SAVE</Button>
                        <Button
                            buttonAction="cancel"
                            href="/posts-page"
                            type="reset"
                        >
                            CANCEL
                        </Button>
                    </div>
                </div>
            </form>
        )
    }
}

function mapStateToProps(state) {
    return {
        post: state.currentPost,
    };
}

export default connect(
    mapStateToProps,
    {getCurrentPost, editPost}
)(Form);
