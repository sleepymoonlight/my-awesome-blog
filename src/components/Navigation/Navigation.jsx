import React, {Component} from 'react';
import PropTypes from "prop-types";

import styles from "./Navigation.module.css";

class Navigation extends Component {

    createButton(page) {
        const buttonClass = `${styles.button} ${styles.pageNumber} ${(page === this.props.currentPage)
            ? styles.buttonActive
            : ''}`;
        return (
            <button
                onClick={this.props.goOnPage.bind(this, page)}
                className={buttonClass}
            >
                {page + 1}
            </button>
        )
    }

    generateNav() {
        let pagesNav = [];
        pagesNav.push(this.createButton(0));
        if (this.props.currentPage - 5 <= 0) {
            const reachedMaxPage = (this.props.maxPages <= 5);
            let endPage = 5;
            if (reachedMaxPage) {
                endPage = this.props.maxPages;
            }
            for (let i = 1; i < endPage; i++) {
                pagesNav.push(this.createButton(i));
            }
            if (!reachedMaxPage) {
                pagesNav.push(
                    <button className={styles.button}>...</button>
                );
            }
        } else if (this.props.currentPage + 3 >= this.props.maxPages) {
            pagesNav.push(
                <button className={styles.button}>...</button>
            );
            for (let i = this.props.maxPages - 5; i < this.props.maxPages; i++) {
                pagesNav.push(this.createButton(i));
            }
        } else {
            pagesNav.push(
                <button className={styles.button}>...</button>
            );
            for (let i = this.props.currentPage - 1; i < this.props.currentPage + 2; i++) {
                pagesNav.push(this.createButton(i));
            }
            pagesNav.push(
                <button className={styles.button}>...</button>
            );
            pagesNav.push(this.createButton(this.props.maxPages - 1));
        }

        return pagesNav;
    }

    render() {
        const previousButtonClass = `${styles.button} ${(this.props.currentPage === 0)
            ? styles.buttonDisabled
            : ''}`;
        const nextButtonClass = `${styles.button} ${styles.lastButton} ${(this.props.currentPage === this.props.maxPages - 1)
            ? styles.buttonDisabled
            : ''}`;
        return (
            <div className={styles.navContainer}>
                <div className={styles.navigation}>
                    <button
                        onClick={this.props.goOnPage.bind(this, this.props.currentPage - 1)}
                        className={previousButtonClass}
                    >
                        Previous
                    </button>
                    {this.generateNav()}
                    <button
                        onClick={this.props.goOnPage.bind(this, this.props.currentPage + 1)}
                        className={nextButtonClass}
                    >
                        Next
                    </button>
                </div>
            </div>
        );
    }
}

Navigation.propTypes = {
    goOnPage: PropTypes.func.isRequired,
    currentPage: PropTypes.number.isRequired,
    maxPages: PropTypes.number.isRequired

};

export default Navigation;
