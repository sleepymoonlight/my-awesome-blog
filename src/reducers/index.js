import {
    LOAD_POSTS,
    LOAD_ACTIVE_POSTS,
    DELETE_POST,
    EDIT_POST,
    LOAD_COMMENTS,
    LOAD_CURRENT_POST
} from '../constants/action-types';

const initialState = {
    posts: [],
    activePosts: [],
    activeComments: [],
    currentPost: {},
    maxPages: 0
};

function rootReducer(state = initialState, action) {
    if (action.type === LOAD_POSTS) {
        return Object.assign({}, state, {
            posts: state.posts.concat(action.payload)
        })
    }

    if (action.type === LOAD_ACTIVE_POSTS) {
        return Object.assign({}, state, {
            activePosts: action.payload.activePosts,
            maxPages: action.payload.maxPages
        })
    }

    if (action.type === LOAD_CURRENT_POST) {
        return Object.assign({}, state, {
            currentPost: action.payload
        })
    }

    if (action.type === DELETE_POST) {
        return Object.assign({}, state, {
            posts: action.payload
        })
    }

    if (action.type === EDIT_POST) {
        return Object.assign({}, state, {
            posts: action.payload
        })
    }

    if (action.type === LOAD_COMMENTS) {
        return Object.assign({}, state, {
            activeComments: action.payload
        })
    }

    return state;
}

export default rootReducer;
