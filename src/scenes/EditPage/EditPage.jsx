import React from 'react';

import Title from "../../components/Title/Title";
import Form from "../../components/Form/Form";

import styles from "./EditPage.module.css";

export class EditPage extends React.Component {
    render() {
        return (
            <div className={styles.formContainer}>
                <Title isItMainTitle={true}>EDITING POST</Title>
                <Form id={this.props.match.params.id}/>
            </div>
        )
    }
}