import React, {Component} from "react";
import Title from "../../components/Title/Title";
import PostsList from "../../components/PostsList/PostsList";

import styles from "./PostsPage.module.css"

class PostsPage extends Component {
    render() {
        return (
            <div className={styles.homeContainer}>
                <Title isItMainTitle={true}>MY AWESOME BLOG</Title>
                <PostsList/>
            </div>
        );
    }
}

export default PostsPage;
