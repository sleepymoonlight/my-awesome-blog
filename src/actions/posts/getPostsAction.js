import {
    LOAD_POSTS
} from "../../constants/action-types";

export function getPostsAction() {
    return function (dispatch) {
        return fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(json => {
                dispatch({type: LOAD_POSTS, payload: json});
            });
    };
}
