import { DELETE_POST } from "../../constants/action-types";

export function deletePostAction(post) {
    return function (dispatch, getState) {
        const posts = getState().posts;
        posts.splice(posts.indexOf(post), 1);
        fetch('https://jsonplaceholder.typicode.com/posts/' + post.id, {
            method: 'DELETE'
        });
        dispatch({type: DELETE_POST, payload: posts})
    };
}
