import {
    LOAD_ACTIVE_POSTS
} from "../../constants/action-types";

export function showActivePosts(page, postsOnPage) {
    return function (dispatch, getState) {
        const posts = getState().posts;
        const endIndex = (((page + 1) * postsOnPage) < posts.length) ? ((page + 1) * postsOnPage) : posts.length - 1;
        const activePosts = posts.slice(page * postsOnPage, endIndex);
        const maxPages = posts.length / postsOnPage;

        dispatch({type: LOAD_ACTIVE_POSTS, payload: {activePosts: activePosts, maxPages: maxPages}});
    }
}
