import { LOAD_CURRENT_POST } from "../../constants/action-types";

export function getCurrentPost(id) {
    return function (dispatch) {
        return fetch('https://jsonplaceholder.typicode.com/posts/' + id)
            .then(response => response.json())
            .then(json => {
                dispatch({type: LOAD_CURRENT_POST, payload: json});
            });
    }
}
