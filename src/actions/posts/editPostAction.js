import { EDIT_POST } from '../../constants/action-types';

export function editPost(id, title, body) {
    return function (dispatch, getState) {
      const posts = getState().posts;
      const editedPost = posts.find(el => el.id.toString() === id.toString());
      editedPost.title = title;
      editedPost.body = body;
         fetch('https://jsonplaceholder.typicode.com/posts/' + id, {
            method: 'PATCH',
            body: JSON.stringify({
                title: title,
                body: body
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => response.json())
            .then(json => {

            });

        dispatch({type: EDIT_POST, payload: posts});
    }
}
