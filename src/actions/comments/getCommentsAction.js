export function showComments(id) {
        return new Promise((resolve, reject) => {
          fetch('https://jsonplaceholder.typicode.com/comments?postId=' + id)
          .then(response => response.json())
          .then(json => {
            resolve(json);
          })
          .catch(error => {
              reject(error);
          })
        });
}
